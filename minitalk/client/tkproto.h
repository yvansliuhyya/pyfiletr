/*
 * MODULE  NAME  :
 * PROGRAM NAME  : proto.h
 * AUTHOR        : hotmocha(hotmocha@126.com)
 * CREATE  DATE  : 2015-10-30 17:27:45
 * PROGRAM DESC  :
 *
 * HISTORY       :
 *
 */
#ifndef _H_PROTO_H_
#define _H_PROTO_H_

#define MSG_TYPE_MEMBER_LIST	0
#define MSG_TYPE_CLIENT_INFO	1
#define MSG_TYPE_COMM_MSG	2	
#define MSG_TYPE_LOGOUT		3

#include "memberlist.dsc.h"

void init_member_list(memberlist *mlist, int cmd_type);
int query_member_byname(memberlist *mlist, struct member *m);
int query_member_byid(memberlist *mlist, struct member *m);
int query_member_byip(memberlist *mlist, struct member *m);
int del_member_byname(memberlist *mlist, struct member *m);
struct member* get_last_member(memberlist *mlist);

#endif
