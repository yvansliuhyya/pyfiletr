#ifndef _H_TKCLIENT_H_
#define _H_TKCLIENT_H_

#include "tkevent.h"
#include "tknetbusi.h"

struct tkclient {
	struct select_env *select_env;
	struct tk_connect *conn;

	int clientid;
	char clientname[50];
        int serverport;
	char serverip[32];

        char logpath[FILEPATHMAXLEN];
        char tkhistory[FILEPATHMAXLEN];
};	

#endif

