/*
 * MODULE  NAME  :
 * PROGRAM NAME  : tkhandler.h
 * AUTHOR        : hotmocha(hotmocha@126.com)
 * CREATE  DATE  : 2015-10-29 10:36:53
 * PROGRAM DESC  :
 *
 * HISTORY       :
 *
 */
#ifndef _H_TKHANDLER_H_
#define _H_TKHANDLER_H_

#include "tknetbusi.h"

int do_after_accept_entry(struct epoll_env *env, struct custom_socket *s);
int do_after_connect_entry(struct custom_socket *s);
int hd_handler_entry(struct epoll_env *env, struct custom_socket *s, int event_type);

#endif
