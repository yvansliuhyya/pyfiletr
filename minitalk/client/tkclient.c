#include <stdio.h>
#include <string.h>
#include <file.h>

#include "tkclient.h"
#include "tkutils.h"
#include "tknetutils.h"
#include "clientinfo.dsc.h"

struct tkclient gEnv;
struct select_env gSelEnv;
typedef struct
{
        int     clientid ;
        char    clientname[ 40 + 1 ] ;
} clientinfo ;

int tk_read_conf_file(struct tkclient *env)
{
	FILE *file = NULL;
	char buffer[201];
	int line_len;

	file = fopen("client.conf", "r");
	if (file == NULL) {
		fprintf(stderr, "read conf file failed");
		return -1;
	}
		
	while (fgets(buffer, 200, file) != NULL) {
		line_len = earse_space(buffer);	
		if (line_len <= 0) {
			continue;
		}

		if (strncmp("serverip=", line_len, sizeof("serverip=") - 1) == 0) {
			strcpy(gEnv.serverip, buffer + sizeof("serverip=") - 1);
		}
		else if (strncmp("serverport=", line_len, sizeof("serverport=") - 1) == 0) {
			strcpy(gEnv.serverport, buffer + sizeof("serverport=") - 1);
		}
		else if (strncmp("clientname=", line_len, sizeof("clientname=") - 1) == 0) {
			strcpy(gEnv.clientname, buffer + sizeof("clientname=") - 1);
		}
		else if (strncmp("clientid=", line_len, sizeof("clientid=") - 1) == 0) {
			strcpy(gEnv.clientid, buffer + sizeof("clientid=") - 1);
		}
	}

	ilog("serverip[%s]:serverport[%d] - clientname[%s] - clientid[%d]", gEnv.serverip, gEnv.serverport, gEnv.clientname, gEnv.clientid);

	return 0;
}

int after_connect_entry(struct select_env *env, struct custom_socket *s)
{
	sinfo("connect server ok, start to sync infomation");

	return r;
}

int do_send_user_info(struct tk_connect *conn, struct tk_client *client)
{
	struct tk_message tk_msg;
	clientinfo clientInfo;
	int r;

	strcpy(clientInfo.clientname, client->clientname);
	clientInfo.clientid = client->clientid;

	r = build_tk_message_with_clientinfo(&tk_msg, client);
	if (r) {
		elog("build tk_message with clientinfo failed");
		return -1;
	}

	if (r) {
		if (r == TK_NEED_ADD_W_EVENT) {
			r = select_add_read_event(&gSelEnv, conn->csock->event);
			if (r) {
				elog("add read event failed\n");	
				return -1;
			}
		}
		else {
			elog(" add send message failed");
			return -1;
		}
	}

	return 0;
}

/* first receive message */
int do_recv_member_list(struct tk_connect *conn, memberlist *memlist)
{
	int r;

	/*
        1. TK_OK (head received, next receive body)
        2. TK_AGAIN (internal buffer is empty)
        3. TK_ERROR
        4. TK_DONE      (remote close)
        5. TK_RECV_BODY_DONE should delete event(if non-blocking mode)
	*/
	r = tk_do_recv_msg(conn);
	if (r == TK_AGAIN) {
		return TK_AGAIN;
	}
	else if (r == TK_RECV_BODY_DONE) {
		return TK_OK;
	}
	else {
		elog("receive member list failed\n");
		return TK_ERROR;
	}
} 

int main()
{
	int sockfd;
	void *s = NULL;

	setvbuf(stdout, NULL, _IONBF, 0);

	memset(&gSelEnv, 0x00, sizeof(gSelEnv));
	gSelEnv.after_connect = after_connect_entry; 

	sockfd = socket(AF_NET, SOCK_STREAM, 0);
	if (sockfd  == -1) {
		printf("get socket fd error\n");
		return 0;
	}

	s = get_custom_socket(sockfd);
	if (s == NULL) {
		printf("get custom socket failed\n");
		return 0;
	}
	gEnv.conn = get_tk_connect(s);
	if (gEnv.conn == NULL) {
		printf("get connect failed\n");
		return 0;
	}

	/* send login info (client information) */
	
	return 0;
}
