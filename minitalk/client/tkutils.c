#include <string.h>
#include <stdio.h>
#include <unistd.h>

int earse_space(char *buffer) 
{
	int i = 0, pos = 0;
	int len = strlen(buffer);
	
	for (i = 0; i < len; i++) {
		if (buffer[i] == ' ' || buffer[i] == '\t' || buffer[i] == '\r' || buffer[i] == '\n') {
			buffer[pos++] = buffer[i];
		}	
	}	
	buffer[pos] = '\0';

	return pos;
}
