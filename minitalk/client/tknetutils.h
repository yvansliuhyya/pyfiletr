/*
 * MODULE  NAME  :
 * PROGRAM NAME  : netutils.h
 * AUTHOR        : hotmocha(hotmocha@126.com)
 * CREATE  DATE  : 2015-10-21 14:02:13
 * PROGRAM DESC  :
 *
 * HISTORY       :
 *
 */
#ifndef _H_NETUTILS_H_
#define _H_NETUTILS_H_

int set_reuseaddr(int s, int onoff);
int set_tcppush(int s, int onoff);
int set_nonblocking(int s);
int set_tcpkeepalive(int s);

/*
@RETURN:
        1. TK_BUFNOTENOUGH      2. TK_AGAIN
        3. TK_ERROR(errno)      4. TK_DONE(fixme:client use shutdown(SHUT_WR)
        5. TK_TIMEOUT(no use)
*/
int tk_recv(int fd, char *buf, int *buf_len);


/*
   @RETURN:  1. TK_AGAIN        2. TK_ERROR(errno)      *3. TK_DONE(no use)*    4. TK_OK
*/
int tk_send(int fd, char *buf, int *buf_len);


/*
@RETURN:        1. TK_OK
                2. TK_AGAIN
                3. TK_ERROR
                4. TK_DONE
*/

int tk_recvn(int fd, char *buf, int *buf_len);


/*
@RETURN:        1. TK_AGAIN
                2. TK_ERROR(errno)
                *3. TK_DONE(no use)*
                4. TK_OK
*/

int tk_sendn(int fd, char *buf, int *buf_len);

int tk_connect_non_block(int fd, struct sockaddr *sock, int sock_len);

int tk_connect_test(int fd);

#endif
