/*
 * MODULE  NAME  :
 * PROGRAM NAME  : tklog.h
 * AUTHOR        : hotmocha(hotmocha@126.com)
 * CREATE  DATE  : 2015-10-20 14:49:36
 * PROGRAM DESC  :
 *
 * HISTORY       :
 *
 */
#ifndef _H_TKLOG_H_
#define _H_TKLOG_H_

#include <stdio.h>

#define DEBUG   1
#define INFO    2
#define ERROR   3
#define FATAL   4
#define SYSTEM  5

#define TK_LOG_BUF_LEN  2048

extern char tk_log_buf[TK_LOG_BUF_LEN + 1];
extern void GenerateLogMsg(char *filename, int lineno, int log_level);
extern int tk_log_level;
extern FILE *tk_log_file;

FILE* setlogfile(FILE *file);
int setloglevel(int level);

#define sinfo(format, args... )                                                     \
        do {                                                                            \
                int __len__ = 0;                                                        \
                GenerateLogMsg(__FILE__, __LINE__, SYSTEM);                             \
                __len__ = strlen(tk_log_buf);                                           \
                snprintf(tk_log_buf + __len__, TK_LOG_BUF_LEN - __len__, format, ##args); \
                fprintf(tk_log_file, "%s\n", tk_log_buf);                                        \
                if (tk_log_level == DEBUG)                                              \
                        fprintf(stderr, "%s\n", tk_log_buf);                            \
        } while(0)

#define dlog(format, args...)                                                    \
        do {                                                                            \
                if (tk_log_level <= DEBUG) {                                            \
                        int __len__ = 0;                                                \
                        GenerateLogMsg(__FILE__, __LINE__, DEBUG);                      \
                        __len__ = strlen(tk_log_buf);                                   \
                        snprintf(tk_log_buf + __len__, TK_LOG_BUF_LEN - __len__, format, ##args); \
                        fprintf(tk_log_file, "%s\n", tk_log_buf);                               \
                        fprintf(stderr, "%s\n", tk_log_buf);                            \
                }                                                                       \
        } while(0)

#define ilog(format , args... )                                                   \
        do {                                                                            \
                if (tk_log_level <= INFO) {                                             \
                        int __len__ = 0;                                                \
                        GenerateLogMsg(__FILE__, __LINE__, INFO);                       \
                        __len__ = strlen(tk_log_buf);                                   \
                        snprintf(tk_log_buf + __len__, TK_LOG_BUF_LEN - __len__, format, ##args); \
                        fprintf(tk_log_file, "%s\n", tk_log_buf);                               \
                        if (tk_log_level == DEBUG)                                      \
                                fprintf(stderr, "%s\n", tk_log_buf);                    \
                }                                                                       \
        } while(0)

#define elog(format , args... )                                                  \
        do {                                                                            \
                if (tk_log_level <= ERROR) {                                            \
                        int __len__ = 0;                                                \
                        GenerateLogMsg(__FILE__, __LINE__, ERROR);                      \
                        __len__ = strlen(tk_log_buf);                                   \
                        snprintf(tk_log_buf + __len__, TK_LOG_BUF_LEN - __len__, format, ##args); \
                        fprintf(tk_log_file, "%s\n", tk_log_buf);                               \
                        if (tk_log_level == DEBUG)                                      \
                                fprintf(stderr, "%s\n", tk_log_buf);                    \
                }                                                                       \
        } while(0)

#define flog(format , args... )                                                  \
        do {                                                                            \
                int __len__ = 0;                                                        \
                GenerateLogMsg(__FILE__, __LINE__, FATAL);                              \
                __len__ = strlen(tk_log_buf);                                           \
                snprintf(tk_log_buf + __len__, TK_LOG_BUF_LEN - __len__, format, ##args); \
                fprintf(tk_log_file, "%s\n", tk_log_buf);                                       \
                if (tk_log_level == DEBUG)                                              \
                        fprintf(stderr, "%s\n", tk_log_buf);                            \
        } while(0)
#endif
