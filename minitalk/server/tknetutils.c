#define _GNU_SOURCE
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include "tkerror.h"

int set_reuseaddr(int s, int onoff)
{
        if (onoff)
                onoff = 1;

        if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (const void *) &onoff, sizeof (int)) == -1) {
                return -1;
        }

        return 0;
}

int set_tcppush(int s, int onoff)
{
        if (onoff)
                onoff = 1;

        return setsockopt(s, IPPROTO_TCP, TCP_CORK, (const void *) &onoff, sizeof(int));
}

int set_nonblocking(int s)
{
        int     opts;

        opts = fcntl( s, F_GETFL ) ;
        if( opts < 0 )
                return -1;

        opts = opts | O_NONBLOCK;
        if( fcntl( s, F_SETFL , opts ) < 0 ) {
                return -1;
        }

        return 0;
}

int set_keepalive_params(int sockfd)
{
        int keep_idle = 6;   // �~B?^~\?\?0�~R?F~E没�~\~I任�?U?U��~M.���~R,?H~Y�~[�~L?N��?K. 缺�~\~A?@?7200(s)
        int keep_interval = 5;   // ?N��?K?W��~O~Q?N��?K?L~E?Z~D?W��~W��~W��~Z~T�?�~R. 缺�~\~A?@?75(s)
        int keep_count = 2;   // ?N��?K?G~M�~U?Z~D次�~U? ?E��~C��?E?W��~H~Y认�.Z�~^?N�失?U~H.缺�~\~A?@?9(�?

        if (setsockopt(sockfd, SOL_TCP, TCP_KEEPIDLE, &keep_idle, sizeof(int)) < 0) {
                return -1;
        }

        if (setsockopt(sockfd, SOL_TCP, TCP_KEEPCNT, &keep_interval, sizeof(int)) < 0) {
                return -1;
        }

        if (setsockopt(sockfd, SOL_TCP, TCP_KEEPINTVL, &keep_count, sizeof(int)) < 0) {
                return -1;
        }

        return 0;
}

int set_tcpkeepalive(int s)
{
        int on = 1;

        if (set_keepalive_params(s)) {
                return -1;
        }

        if (setsockopt(s, SOL_SOCKET, SO_KEEPALIVE, &on, sizeof(int)) ) {
                return -1;
        }

        return 0;
}


/* 
@RETURN: 
	1. TK_BUFNOTENOUGH      2. TK_AGAIN     
	3. TK_ERROR(errno)      4. TK_DONE(fixme:client use shutdown(SHUT_WR) 
	5. TK_TIMEOUT(no use)
*/
int tk_recv(int fd, char *buf, int *buf_len)
{
	int available = 0;
	int recv_len = 0;
	int r;

	if (buf_len == NULL || *buf_len < 0)
		return -1;

	available = *buf_len;
	for (;;) {
		if (available <= 0) {
			r = TK_BUFNOTENOUGH;
			break;
		}
		
		r = recv(fd, buf + recv_len, available, 0);
		if (r == -1) {
			if (errno == EINTR) 
				continue;
			else {
				if (errno == EAGAIN || errno == EWOULDBLOCK) {
					r = TK_AGAIN;
				}
				else  {
					r = TK_ERROR;
				}
				break;
			}	
		}
		else if (r == 0) {
			r = TK_DONE;
		}
		else {
			recv_len += r;
			available -= available;
		}
	}

	*buf_len = recv_len;

	return r;	
}

/*
   @RETURN:  1. TK_AGAIN        2. TK_ERROR(errno)      *3. TK_DONE(no use)*    4. TK_OK
*/
int tk_send(int fd, char *buf, int *buf_len)
{
	int send_len = 0;
	int remain_len;
	int r;

	if (buf_len == NULL || *buf_len < 0)
		return -1;

	remain_len = *buf_len;

	for (;;) {
		if (remain_len == 0) {
			r = TK_OK;
			break;
		}
		
		r = send(fd, buf + send_len, remain_len, 0);
                if (r == -1) {
                        if (errno == EINTR)
                                continue;
                        else {
                                if (errno ==  EAGAIN || errno == EWOULDBLOCK) {
                                        r = TK_AGAIN;
                                }
                                else {
					r = TK_ERROR;
                                }
                                break;
                        }
                }
		else if (r == 0) {
			r = TK_ERROR;
			break;
		}
		else {
			remain_len -= r;
			send_len += r;
		}
	}
		
	*buf_len = send_len;

	return r;
} 

/*
@RETURN: 	1. TK_OK([a]. r == TK_BUFNOTENOUGH 
		2. TK_AGAIN
		3. TK_ERROR
		4. TK_DONE
*/

int tk_recvn(int fd, char *buf, int *buf_len)
{
	int r = 0;
	int recv_len = *buf_len;

	/*
	@RETURN:
        1. TK_BUFNOTENOUGH      2. TK_AGAIN
        3. TK_ERROR(errno)      4. TK_DONE(fixme:client use shutdown(SHUT_WR)
        5. TK_TIMEOUT(no use)
	*/
	r = tk_recv(fd, buf, buf_len);
	if (r == TK_BUFNOTENOUGH && recv_len == *buf_len) {
		/* when buf not enough, we receive buf_len bytes, so return ok */
		return TK_OK;
	}
	else {
		return r;
	}
}

/*
@RETURN:  	1. TK_AGAIN        
		2. TK_ERROR(errno)      
		*3. TK_DONE(no use)*    
		4. TK_OK
*/

int tk_sendn(int fd, char *buf, int *buf_len)
{
	/*    @RETURN:  1. TK_AGAIN        2. TK_ERROR(errno)      *3. TK_DONE(no use)*    4. TK_OK */
	return tk_send(fd, buf, buf_len);
}

