/*
 * MODULE  NAME  :
 * PROGRAM NAME  : error.h
 * AUTHOR        : hotmocha(hotmocha@126.com)
 * CREATE  DATE  : 2015-10-20 16:58:43
 * PROGRAM DESC  :
 *
 * HISTORY       :
 *
 */
#ifndef _H_ERROR_H_
#define _H_ERROR_H_

#define TK_OK		0
#define TK_ERROR	-1
#define TK_DONE		-2
#define TK_BUFNOTENOUGH	-3
#define TK_AGAIN	-4
#define TK_TIMEOUT	-5
#define TK_JSON_DESERIALIZE	-6
#define TK_JSON_SERIALIZE	-7

#define TK_BODY_TOO_LENGTH	-8
#define TK_TOO_MUCH_SEND_MSG	-9
#define TK_NO_MORE_SEND_MSG	-10
#define TK_NEED_ADD_W_EVENT	-11
#define TK_RECV_HEAD_DONE	-12
#define TK_RECV_BODY_DONE	-13
#define TK_SEND_HEAD_DONE	-14
#define TK_SEND_BODY_DONE	-15

#define TK_TOOMUCH_MEMBER	-16
#define TK_NOTFOUND		-17
#define TK_TOO_LONG_MESSAGE	-18

#endif
