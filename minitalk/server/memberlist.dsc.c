/* It had generated by DirectStruct v1.4.5 */
#include "memberlist.dsc.h"

int DSCINIT_memberlist( memberlist *pst )
{
	int	index[10] = { 0 } ; index[0] = 0 ;
	memset( pst , 0x00 , sizeof(memberlist) );
		for( index[1] = 0 ; index[1] < 1000 ; index[1]++ )
		{
		}
		pst->_nodes_size = 1000 ;
	return 0;
}

#include "fasterjson.h"

int DSCSERIALIZE_JSON_memberlist( memberlist *pst , char *encoding , char *buf , int *p_len )
{
	int	remain_len ;
	int	len ;
	char	tabs[10+1] ;
	int	index[10] = { 0 } ; index[0] = 0 ;
	remain_len = (*p_len) ;
	memset( tabs , '\t' , 10 );
	len=SNPRINTF(buf,remain_len,"{\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"	"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"\"groupid\" : "); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"%d",pst->groupid); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len," ,\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"	"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"\"cmd\" : "); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"%d",pst->cmd); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len," ,\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"	\"nodes\" : \n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"	[\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
			for( index[1] = 0 ; index[1]<pst->_nodes_count ; index[1]++ )
			{
	len=SNPRINTF(buf,remain_len,"	{\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"		\"node\" : \n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"		{\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"			"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"\"clientid\" : "); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"%d",pst->nodes[index[1]].node.clientid); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len," ,\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"			"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"\"clientname\" : "); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"\""); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	JSONESCAPE_EXPAND(pst->nodes[index[1]].node.clientname,strlen(pst->nodes[index[1]].node.clientname),buf,len,remain_len); if(len<0)return -7; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"\""); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len," ,\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"			"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"\"ip\" : "); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"\""); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	JSONESCAPE_EXPAND(pst->nodes[index[1]].node.ip,strlen(pst->nodes[index[1]].node.ip),buf,len,remain_len); if(len<0)return -7; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"\""); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"		}\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
			if((pst->_nodes_count==0)?(index[1]<1000-1):(index[1]<pst->_nodes_count-1))
			{ len=SNPRINTF(buf,remain_len,"	} ,\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len; }
			else
			{ len=SNPRINTF(buf,remain_len,"	}\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len; }
			}
	len=SNPRINTF(buf,remain_len,"	]\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	len=SNPRINTF(buf,remain_len,"}\n"); if(len<0||remain_len<len)return -1; buf+=len; remain_len-=len;
	
	if( p_len ) (*p_len) = (*p_len) - remain_len ;
	
	return 0;
}

funcCallbackOnJsonNode CallbackOnJsonNode_memberlist ;
int CallbackOnJsonNode_memberlist( int type , char *jpath , int jpath_len , int jpath_size , char *node , int node_len , char *content , int content_len , void *p )
{
	memberlist	*pst = (memberlist*)p ;
	int	index[10] = { 0 } ;
	int	len ;
	
	index[0] = 0 ;
	len = 0 ;
	
	if( type & FASTERJSON_NODE_BRANCH )
	{
		if( type & FASTERJSON_NODE_ENTER )
		{
		if( jpath_len == 6 && strncmp( jpath , "/nodes" , jpath_len ) == 0 )
		{if(pst->_nodes_count>=1000)return -8;}
		}
		else if( type & FASTERJSON_NODE_LEAVE )
		{
		if( jpath_len == 6 && strncmp( jpath , "/nodes" , jpath_len ) == 0 )
		{pst->_nodes_count++;}
		}
	}
	else if( type & FASTERJSON_NODE_LEAF )
	{
		/* groupid */
		if( jpath_len == 8 && strncmp( jpath , "/groupid" , jpath_len ) == 0 )
		{NATOI(content,content_len,pst->groupid);}
		/* cmd */
		if( jpath_len == 4 && strncmp( jpath , "/cmd" , jpath_len ) == 0 )
		{NATOI(content,content_len,pst->cmd);}
				/* clientid */
				if( jpath_len == 20 && strncmp( jpath , "/nodes/node/clientid" , jpath_len ) == 0 )
				{NATOI(content,content_len,pst->nodes[pst->_nodes_count].node.clientid);}
				/* clientname */
				if( jpath_len == 22 && strncmp( jpath , "/nodes/node/clientname" , jpath_len ) == 0 )
				{JSONUNESCAPE_FOLD(content,content_len,pst->nodes[pst->_nodes_count].node.clientname,len,sizeof(pst->nodes[pst->_nodes_count].node.clientname)-1); if(len<0)return -7;}
				/* ip */
				if( jpath_len == 14 && strncmp( jpath , "/nodes/node/ip" , jpath_len ) == 0 )
				{JSONUNESCAPE_FOLD(content,content_len,pst->nodes[pst->_nodes_count].node.ip,len,sizeof(pst->nodes[pst->_nodes_count].node.ip)-1); if(len<0)return -7;}
	}
	
	return 0;
}

int DSCDESERIALIZE_JSON_memberlist( char *encoding , char *buf , int *p_len , memberlist *pst )
{
	char	jpath[ 1024 + 1 ] ;
	int	nret = 0 ;
	memset( jpath , 0x00 , sizeof(jpath) );
	nret = TravelJsonBuffer( buf , jpath , sizeof(jpath) , & CallbackOnJsonNode_memberlist , (void*)pst ) ;
	if( nret )
		return nret;
	
	return 0;
}
