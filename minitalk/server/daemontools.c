#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <signal.h>
#include "daemontools.h"

int todaemon()
{
	pid_t pid;
	int sig;

	pid = fork();
	switch(pid) {
		case -1:
			return -1;	
		case 0:		/* in child */
			break;
		default:
			exit(0);
			break;
	}

	setsid();
				
	pid = fork();
	switch(pid) {
		case -1:
			return -2;	
		case 0:
			break;
		default:
			exit(0);
			break;
	}

	setuid(getpid());
	umask(0);
	for (sig = 0; sig <= 64; sig++) {
		signal(sig, SIG_IGN);
	}

	return 0;
}
