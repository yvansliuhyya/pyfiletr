/*
 * MODULE  NAME  :
 * PROGRAM NAME  : constants.h
 * AUTHOR        : hotmocha(hotmocha@126.com)
 * CREATE  DATE  : 2015-10-20 14:57:50
 * PROGRAM DESC  :
 *
 * HISTORY       :
 *
 */
#ifndef _H_CONSTANTS_H_
#define _H_CONSTANTS_H_

#define VOID void
#define FUNCTION 

#define FILEPATHMAXLEN	256
#define MAXCONNECTING	100
#define IPV4LEN		20
#define MSGMAXLEN	2048
#define EPOLL_MAX_WAITEVENTS_NUM	512
#define MAXIOBUFLEN	4096
#define	HEADERLEN	4
#define MAXMESSAGELEN	4096
#define MAXMESSAGENUM	20

#define MEMBER_CMD_INIT		0
#define MEMBER_CMD_ADD		1
#define MEMBER_CMD_DELETE	2

#endif
