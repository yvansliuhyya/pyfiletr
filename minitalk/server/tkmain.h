/*
 * MODULE  NAME  :
 * PROGRAM NAME  : main.h
 * AUTHOR        : hotmocha(hotmocha@126.com)
 * CREATE  DATE  : 2015-10-20 14:56:34
 * PROGRAM DESC  :
 *
 * HISTORY       :
 *
 */
#ifndef _H_MAIN_H_
#define _H_MAIN_H_

#include "tkconstants.h"
#include "tknetbusi.h"
#include "tkevent.h"
#include "memberlist.dsc.h"
#include "tklist.h"

struct config {
	char logpath[FILEPATHMAXLEN];
	int serverport;
	int maxconnecting;
	char tkhistory[FILEPATHMAXLEN];
};

struct session {
	char clientip[IPV4LEN];
	int clientport;
	char sendbuffer[10][MSGMAXLEN];		/* max store 5 messages */
	char recvbuffer[MSGMAXLEN];		/* client use sync send */
};

struct serverenv {
	struct epoll_env *epoll_env;
	int listenfd;

	struct config *conf;

	int connectnum;
	struct list_head *pclients_list;	/* put struct tk_connect */
	memberlist member_list;
};

#endif
